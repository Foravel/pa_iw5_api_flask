import time
from flask import Flask
from flask import request
#pip3 install beautifulsoup4 requests elasticsearch
import random
from bs4 import BeautifulSoup
import requests
from elasticsearch import Elasticsearch
from flask import jsonify
from pprint import pprint
import spacy
import os
from dotenv import load_dotenv

load_dotenv()

ELASTICSEARCH_DATABASE_ENDPOINT=os.getenv('ELASTICSEARCH_DATABASE_ENDPOINT')
nlp = spacy.load("en_core_web_sm") 
app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def hello():
    headers = {'User-Agent': GET_UA()}
    url = "https://newsapi.org/v2/everything?q=tesla&sortBy=publishedAt&apiKey=9309ec6722c94258b23fbb00b8dee698&language=en"
    response = requests.get(url, headers=headers)
    jsonResponse = response.json()
    print(jsonResponse)
    return jsonify(jsonResponse)

def GET_UA():
    uastrings = ["Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.72 Safari/537.36",
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10) AppleWebKit/600.1.25 (KHTML, like Gecko) Version/8.0 Safari/600.1.25",
                "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0",
                "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36",
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36",
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/600.1.17 (KHTML, like Gecko) Version/7.1 Safari/537.85.10",
                "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko",
                "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.104 Safari/537.36"
                ]
 
    return random.choice(uastrings)


def remove_named_entities(text, doc):
    for ent in doc.ents:
        text = text.replace(ent.text, '[NAMEDENTITY:'+ent.label_+']'+ent.text.replace(' ', '_')+'[/NAMEDENTITY]')
    return text

@app.route('/scrap', methods=['GET', 'POST'])
def scrap():
    es = Elasticsearch([ELASTICSEARCH_DATABASE_ENDPOINT])

    while not es.ping():
        time.sleep(5)

    headers = {'User-Agent': GET_UA()}

    campaign_keywords = request.get_json().get('campaign_keywords')
    campaign_language = request.get_json().get('campaign_language')


    headers = {'User-Agent': GET_UA()}
    for keyword in campaign_keywords:
    #return jsonify([json.dumps(request.get_json().get('campaign_keywords'))])
        url = "https://newsapi.org/v2/everything?q="+keyword+"&sortBy=publishedAt&apiKey=9309ec6722c94258b23fbb00b8dee698&language="+campaign_language+""
        response = requests.get(url, headers=headers)
        jsonResponse = response.json()
        articles_urls = []

    for article in jsonResponse['articles']:
        articles_urls.append(article['url'])

    for url in articles_urls:
        response = requests.get(url, headers=headers)
        soup = BeautifulSoup(response.text)
        all_p = [p.getText().strip() for p in soup.find_all('p') if len(p.getText()) > 15]
        
        for p in all_p:
            doc_nlp = nlp(p)
            p = remove_named_entities(p, doc_nlp)
            #campaign_id = 9000
            campaign_id = request.get_json().get('campaign_id')
            doc_elk = {'campaign_id': campaign_id,'text': p}
            es.index(index="text", body=doc_elk)
            
        return jsonify(articles_urls)

@app.route('/test', methods=['GET', 'POST'])
def test():
    
    """ text = "Apple is looking at buying U.K. startup for $1 billion"
    doc = nlp(text)

    for ent in doc.ents:
        print(ent.text, ent.start_char, ent.end_char, ent.label_)
        text = text.replace(ent.text, '[#NAMED ENTITY#]')
     """

    es = Elasticsearch([ELASTICSEARCH_DATABASE_ENDPOINT])

    while not es.ping():
        time.sleep(5)

    doc_elk = {'campaign_id': 1005,'text': 'TESTESTEST'}
    es.index(index="text", body=doc_elk)
    return jsonify(ELASTICSEARCH_DATABASE_ENDPOINT)

if __name__ == "__main__":
	app.run()

